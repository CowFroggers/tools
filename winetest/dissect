#!/usr/bin/perl
#
# Copyright (C) 2004 Ferenc Wagner
# Copyright (C) 2008 Alexandre Julliard
# Copyright (C) 2008-2021 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

use strict;
use warnings;

use open ':utf8';
use CGI qw(:standard);
charset("utf-8");

sub BEGIN
{
    if ($0 !~ m=^/=)
    {
        # Turn $0 into an absolute path so it can safely be used in @INC
        require Cwd;
        $0 = Cwd::cwd() . "/$0";
    }
    unshift @INC, $1 if ($0 =~ m=^(/.*)/[^/]+$=);
}
use vars qw/$workdir $gitdir $gitlab $maxmult $maxuserskips $maxfailedtests $maxunitsize $maxfilesize $acceptprediluvianwin/;
require "winetest.conf";

my $name0=$0;
$name0 =~ s+^.*/++;

# We support reports version 4 and up
my $minimum_report_version=4;
# And we generate summary files version 4
my $summary_version=5;


#
# Common helpers
#

sub error(@)
{
    print STDERR "$name0:error: ", @_;
}

$ENV{GIT_DIR} = $gitdir;

sub get_build_info($)
{
    my ($build) = @_;
    my ($date, $subject);

    my $commit = `git log --max-count=1 --pretty="format:%ct %s" "$build^0" 2>/dev/null` if ($build =~ /^[0-9a-f]{40}$/);
    if ($commit && $commit =~ /^(\d+) (.*)$/)
    {
        ($date, $subject) = ($1, $2);
        # Make sure the directory's mtime matches the commit time
        utime $date, $date, "data/$build";
    }
    else
    {
        $date = (stat "data/$build")[9];
        $subject = "";
    }
    return ($date, $subject);
}

use POSIX qw(locale_h strftime);
setlocale(LC_ALL, "C");

sub short_date($)
{
    my ($date) = @_;
    return strftime("%b %d", gmtime($date));
}


#
# Command line processing
#

my ($opt_workdir, $opt_maxfails, $update, $report, $usage);

sub check_opt_val($$)
{
    my ($option, $val) = @_;

    if (defined $val)
    {
        error("$option can only be specified once\n");
        $usage = 2; # but continue processing this option
    }
    if (!@ARGV)
    {
        error("missing value for $option\n");
        $usage = 2;
        return undef;
    }
    return shift @ARGV;
}

while (@ARGV)
{
    my $arg = shift @ARGV;
    if ($arg eq "--workdir")
    {
        $workdir = $opt_workdir = check_opt_val($arg, $opt_workdir);
    }
    elsif ($arg eq "--update")
    {
        $report = check_opt_val($arg, $report);
        $update = 1;
    }
    elsif ($arg eq "--max-fails")
    {
        $opt_maxfails = check_opt_val($arg, $opt_maxfails);
    }
    elsif ($arg eq "--help")
    {
        $usage = 0;
    }
    else
    {
        error("unknown argument '$arg'\n");
        $usage = 2;
    }
}
if (!defined $usage)
{
    if (!defined $workdir)
    {
        require Cwd;
        $workdir = Cwd::cwd();
    }
    elsif ($workdir !~ m%^/%)
    {
        require Cwd;
        $workdir = Cwd::cwd() . "/$workdir";
    }
    if (defined $opt_maxfails)
    {
        $maxfailedtests = $opt_maxfails;
        if ($opt_maxfails !~ /^\d+$/)
        {
            error("'$opt_maxfails' must be a positive integer\n");
            $usage = 2;
        }
    }
    if (!-f "$workdir/report.css")
    {
        error("'$workdir' is not a valid work directory\n");
        $usage = 2;
    }
    if (defined $report and !-f $report)
    {
        error("the '$report' report is not valid\n");
        $usage = 2;
    }
}
if (defined $usage)
{
    if ($usage)
    {
        error("try '$name0 --help' for more information\n");
        exit $usage;
    }
    print <<EOF;
Usage: $name0 [--workdir DIR] [--update REPORT] [--max-fails MAX] [--help]

Processes a test report to generate the corresponding HTML files.

Where:
  --workdir DIR     Specifies the directory containing the winetest website
                    files. Can be omitted if set in winetest.conf.
  --update REPORT   Updates the HTML files of the specified test report. Note
                    that it must have already been moved into place.
  --max-fails MAX   Reject reports with more than MAX failed test units.
  --help            Shows this usage message.

Actions:
  $name0 looks for a WineTest report file matching queue/rep*/report,
  takes it apart in its directory while also creating summary.txt. If an error
  occurs the directory is renamed to errXXXXX to avoid future attempts at
  processing this report.

  If everything goes flawlessly the whole directory is renamed (based on the
  information learned in the process) to data/BUILD/PLATFORM_TAG_DIGIT
  where DIGIT is for resolving name clashes and data/BUILD/outdated is
  created to signal the change in the given build.

Generated files:
  \$workdir/queue/errXXXXX/error
  \$workdir/queue/errXXXXX/summary.txt
  or
  \$workdir/data/BUILD/outdated
  \$workdir/data/BUILD/PLATFORM_TAG_DIGIT/summary.txt
  \$workdir/data/BUILD/PLATFORM_TAG_DIGIT/report.html
  \$workdir/data/BUILD/PLATFORM_TAG_DIGIT/TEST:UNIT.html

Exit:
  0 - successfully processed a report, call again
  1 - failed to process a report, call again
  2 - there was nothing to do or usage error
  3 - fatal error, something went utterly wrong
EOF
    exit 0;
}

chdir($workdir) or die "could not chdir to the work directory: $!";

if (!defined $report)
{
    ($report, undef) = glob "$workdir/queue/rep*/report";
    exit 2 if (!defined $report or !-f $report);
}

my $tmpdir = $report;
$tmpdir =~ s|^(.+)/report$|$1|;


#
# Check the report version, build id and tag
#

use File::Temp qw/tempdir/;

my $tag;
sub mydie(@)
{
    my $label = $tag ? $tag : "<notag>";
    if (!$update) {
        my $errdir = tempdir ("errXXXXX", DIR => "$workdir/queue");
        if (!rename $tmpdir, $errdir) {
            error("could not rename '$tmpdir' to '$errdir': $!\n");
            exit 3;
        }
        if (open ERR, ">$errdir/error")
        {
            print ERR "$label: ", @_, "\n";
            close ERR;
        }
    }
    print STDERR "$name0:error:$label: ", @_, "\n";
    exit 1;
}

open IN, "<:raw", $report or mydie "could not open '$report' for reading: $!";

# summary.txt file format:
# Version <version>
# - <dll> - missing - - - -
# - <dll> - missing(dll|entrypoint|ordinal|sxs) - - - -
# - <dll> - (loaderror<code>|native|stub) - - - -
# - <dll> - skipped - - - -
# - <dll> <unit> skipped - - - <source>
# - <dll> <unit> failed (258|crash) - - <source>
# - <dll> <unit> <total> <todo> (<failures>|big) <skipped> <source>
open SUM, ">$tmpdir/summary.txt" or mydie "could not open '$tmpdir/summary.txt' for writing: $!";

my $line = <IN> || "";
$line =~ /^Version (\d+)\r?$/ or mydie "no version header: $line";
mydie "illegal version: $1" if ($1 lt $minimum_report_version);
print SUM "Version $summary_version\n";

$line = <IN> || "";
$line =~ /^Tests from build ([-.0-9a-zA-Z]+)\r?$/ or mydie "no build header: $line";
my $testbuild = $1;
$testbuild =~ /^[0-9a-f]{40}$/ or mydie "not a valid commit id $testbuild";
my $commit = `git rev-parse --verify $testbuild^0 2>/dev/null`;
chomp $commit;
$testbuild eq $commit or mydie "not an existing commit $testbuild";
my $shortbuild = substr($testbuild,0,12);

my ($date, $_subject) = get_build_info($testbuild);
my $short_date = short_date($date);

$line = <IN> || "";
$line = <IN> || "" if ($line =~ /^Archive: /); # Ignore the Archive header

$line =~ /^Tag: ([-.0-9a-zA-Z]*)\r?$/ or mydie "no tag line: $line";
$tag = $1;


#
# Parse and check the report header
#

my @boxes;

sub create_box($$$)
{
    my ($id, $class, $title) = @_;
    my $box = { id => $id, class => $class, title => $title, data => "" };
    push @boxes, $box;
    return $box;
}

$line = <IN> || "";
$line =~ /^Build info:\r?$/ or mydie "no build info header: $line";
my $box = create_box( "version", "version", "$tag $short_date information" );
$box->{data} .= "<h2>Build version</h2>\n";
$box->{data} .= "<table class=\"output\">\n";
$box->{data} .= "<tr><td>Build</td><td><a title=\"$testbuild\" href=\"$gitlab/commit/$testbuild\">$shortbuild</a></td></tr>\n";
$box->{data} .= "<tr><td>Tag</td><td><a title=\"Full report\" href=\"report.html\">$tag</a></td></tr></table>\n";
$box->{data} .= "<div class=\"output\"> </div>\n";
while ($line = <IN> || "")
{
    last if ($line !~ s/^    //);
    chomp $line;
    $line =~ s/\r+$//;
    $box->{data} .= "<div class=\"output\">" . escapeHTML($line) . "</div>\n";
}

$line =~ /^Operating system version:\r?$/ or mydie "no OS header: $line";
$box->{data} .= "<h2>Operating system version</h2>\n";
$box->{data} .= "<table class=\"output\">\n";

my ($wine, $wine_build, $major, $minor, $winbuild, $plid, $product, $host);
while ($line = <IN> || "")
{
    last if ($line !~ /^\s*([0-9a-zA-Z ]+)=(.*?)\r?$/);
    if ($1 eq "URL") {
        $box->{data} .= sprintf "<tr><td>$1</td><td><a href=\"%s\">%s</a></td></tr>\n", escapeHTML($2), escapeHTML($2);
    } else {
        $box->{data} .= sprintf "<tr><td>$1</td><td>%s</td></tr>\n", escapeHTML($2);
    }
    if      ($1 eq "bRunningUnderWine") {
        $wine = $2;
    } elsif ($1 eq "dwMajorVersion") {
        $major = $2;
    } elsif ($1 eq "dwMinorVersion") {
        $minor = $2;
    } elsif ($1 eq "dwBuildNumber") {
        $winbuild = $2;
    } elsif ($1 eq "PlatformId") {
        $plid = $2;
    } elsif ($1 eq "wProductType") {
        $product = $2;
    } elsif ($1 eq "WineBuild") {
        $wine_build = $2;
    } elsif ($1 eq "Host system") {
        $host = $2;
    }
}
$box->{data} .= "</table>\n";

if (!defined $plid or !defined $major or !defined $minor or !defined $product) {
    mydie "missing a PlatformId, dwMajorVersion, dwMinorVersion or wProductType field";
}

my @idmatch = (
    # Describes how to match a platform's version information
    # with a dissect platform id:
    # dissect_id  plid  major  minor  build  product prediluvian
    [ "95",          1,     4,     0, undef, undef,  1 ],
    [ "98",          1,     4,    10, undef, undef,  1 ],
    [ "me",          1,     4,    90, undef, undef,  1 ],
    [ "nt3",         2,     3,    51, undef, undef,  1 ],
    [ "2000",        2,     5,     0, undef, undef,  1 ],
    [ "xp",          2,     5,     1, undef,     1,  1 ],
    [ "xp",          2,     5,     2, undef,     1,  1 ],
    [ "2003",        2,     5,     2, undef, undef,  1 ],
    [ "vista",       2,     6,     0, undef,     1,  1 ],
    [ "2008",        2,     6,     0, undef,     3,  1 ],
    [ "win7",        2,     6,     1, undef,     1,  0 ],
    [ "2008",        2,     6,     1, undef,     3,  0 ],
    [ "win8",        2,     6,     2, undef, undef,  0 ],
    [ "win81",       2,     6,     3, undef, undef,  0 ],
    [ "win1507",     2,    10,     0, 10240,     1,  0 ],
    [ "win1511",     2,    10,     0, 10586,     1,  0 ],
    [ "win1607",     2,    10,     0, 14393,     1,  0 ],
    [ "win1703",     2,    10,     0, 15063,     1,  0 ],
    [ "win1709",     2,    10,     0, 16299,     1,  0 ],
    [ "win1803",     2,    10,     0, 17134,     1,  0 ],
    [ "win1809",     2,    10,     0, 17763,     1,  0 ],
    [ "win1903",     2,    10,     0, 18362,     1,  0 ],
    [ "win1909",     2,    10,     0, 18363,     1,  0 ],
    [ "win2004",     2,    10,     0, 19041,     1,  0 ],
    [ "win2009",     2,    10,     0, 19042,     1,  0 ],
    [ "win21H1",     2,    10,     0, 19043,     1,  0 ],
    [ "win21H2",     2,    10,     0, 19044,     1,  0 ],
#   [ "ce",          3, undef, undef, undef, undef,  0 ],
    [ "unknown",     2, undef, undef, undef, undef,  0 ],
    [ "unknown", undef, undef, undef, undef, undef,  1 ],
);

my ($version, $prediluvian);
foreach my $entry (@idmatch)
{
    if ((!defined $entry->[1] or $entry->[1] eq $plid) and
        (!defined $entry->[2] or $entry->[2] eq $major) and
        (!defined $entry->[3] or $entry->[3] eq $minor) and
        (!defined $entry->[4] or $entry->[4] eq $winbuild) and
        (!defined $entry->[5] or $entry->[5] eq $product))
    {
        $version = $entry->[0];
        $prediluvian = $entry->[6];
        last;
    }
}
# Give a little slack to the latest Windows 10.
$maxfailedtests += 20 if ($version =~ /^win200[49]$/);

if ($prediluvian and not $acceptprediluvianwin)
{
    mydie "platform $version (platform $plid, type $product, $major.$minor) not accepted";
}

if ($wine) {
    my %known_hosts = ( "Linux" => "linux",
                        "Darwin" => "mac",
                        "FreeBSD" => "bsd",
                        "SunOS" => "solaris" );
    $version = $known_hosts{$host || ""} || "wine";
}
if ($wine_build) {
    my $wine_commit;
    if ($wine_build =~ /-g([0-9a-f]+)$/)
    {
        $wine_commit = `git rev-parse --verify $1^0 2>/dev/null`;
    }
    elsif ($wine_build =~ /^[-+._0-9A-Za-z]+$/)
    {
        $wine_commit = `git rev-parse --verify $wine_build^0 2>/dev/null`;
    }
    else
    {
        mydie "invalid wine build '$wine_build'";
    }
    chomp $wine_commit;
    mydie "unknown wine build '$wine_build'" unless $wine_commit;
    my $merge_base = `git merge-base $wine_commit $testbuild 2>/dev/null`;
    chomp $merge_base;
    $merge_base eq $testbuild or mydie "wine build '$wine_build' not a descendant of build $testbuild";
}


#
# Parse the 'Dll info' section
#

$line =~ /^Dll info:\r?$/ or mydie "no Dll info header: $line";
$box->{data} .= "<h2>DLL version</h2>\n";

my $skipped_units;
my %dllinfo;
while ($line = <IN> || "")
{
    last if ($line !~ /^\s+([^ =]+)=(.*?)\r?$/);
    my ($dll, $info) = ($1, $2);
    $dllinfo{$dll} = { version => $info };
    if ($info =~ /^dll is missing an ordinal/)
    {
        print SUM "- $dll - missingordinal - - - -\n";
    }
    elsif ($info =~ /^dll is missing an entrypoint/)
    {
        print SUM "- $dll - missingentrypoint - - - -\n";
    }
    elsif ($info =~ /^dll is missing the requested side-by-side version/)
    {
        print SUM "- $dll - missingsxs - - - -\n";
    }
    elsif ($info =~ /^dll is missing/)
    {
        print SUM "- $dll - missingdll - - - -\n";
    }
    elsif ($info =~ /^dll is native/)
    {
        print SUM "- $dll - native - - - -\n";
    }
    elsif ($info =~ /^dll is a stub/)
    {
        print SUM "- $dll - stub - - - -\n";
    }
    elsif ($info =~ /^load error \d+$/)
    {
        $info =~ s/ //g;
        print SUM "- $dll - $info - - - -\n";
    }
    elsif (# For compatibility with old WineTest versions
           $info =~ /^(?:failed|unknown|version not available)$/)
    {
        print SUM "- $dll - missing - - - -\n";
    }
    elsif ($info eq "skipped")
    {
        print SUM "- $dll - skipped - - - -\n";
        mydie "too many dlls skipped by user request (>$maxuserskips at $dll)" if ++$skipped_units > $maxuserskips;
    }
    # The version errors don't prevent the test from running so do not report
    # them.
}


#
# Parse the tests output
#

my ($dll, $unit, $units_re, $source, $result) = ("", "", "", "");
my $unitsize = 0;
my %units;
my ($failures, $todo, $skipped) = (0, 0, 0);
my ($s_failures, $s_todo, $s_skipped, $s_total) = (0, 0, 0, 0);
my (%pids, $rc, $summary, $broken, $ignore_exceptions);
my ($extra_failures, $failed_units) = (0, 0);

sub get_source_link($$)
{
    my ($_unit, $_lnum) = @_;
    # Let the caller check $_unit to not interfere with check_unit()

    my $source_link = defined $_unit ? "$_unit.c" : $source ne "-" ? $source : "$dll:$unit";
    $source_link .= ":$_lnum" if (defined $_lnum);
    if ($source ne "-")
    {
        my $url = "$gitlab/blob/$testbuild/$source";
        $url .= "#L$_lnum" if (defined $_lnum);
        $source_link = "<a href=\"$url\">$source_link</a>";
    }
    return $source_link;
}

my $testbox;

sub add_test_line($$)
{
    my ($class, $line) = @_;
    $testbox->{data} .= "<div class=\"test $class\">$line</div>\n";
}

sub check_unit($$)
{
    my ($l_unit, $l_type) = @_;
    if (!$units{$l_unit} and !$broken)
    {
        add_test_line("end", "Misplaced $l_type line");
        $extra_failures++;
        $broken = 1;
    }
}

sub check_summary_counter($$$)
{
    my ($count, $s_count, $type) = @_;

    if ($count != 0 and $s_count == 0)
    {
        add_test_line("end", "The test has unaccounted for $type messages");
        $extra_failures++;
    }
    elsif ($count == 0 and $s_count != 0)
    {
        add_test_line("end", "The test is missing some $type messages");
        $extra_failures++;
    }
}

sub create_test_unit_box()
{
    if (defined($dllinfo{$dll}->{version}) && !$dllinfo{$dll}->{first})
    {
        $dllinfo{$dll}->{first} = "$dll:$unit";
    }
    my $box = create_box("$dll:$unit", "testfile", get_source_link(undef, undef));
    $box->{testname} = "$dll:$unit";
    return $box;
}

sub close_test_unit($)
{
    my ($last) = @_;

    # Verify the counters
    if (!$broken)
    {
        check_summary_counter($failures, $s_failures, "failure");
        check_summary_counter($todo, $s_todo, "todo");
        check_summary_counter($skipped, $s_skipped, "skip");
    }

    # Note that the summary lines may count some failures twice
    # so only use them as a fallback.
    $failures ||= $s_failures;
    $todo ||= $s_todo;
    $skipped ||= $s_skipped;

    my $toobig;
    if ($unitsize > $maxunitsize)
    {
        add_test_line("end", "The test prints too much data ($unitsize bytes)");
        $extra_failures++;
        $toobig = 1;
    }
    if (!$broken and defined $rc)
    {
        # Check the exit code, particularly against failures reported
        # after the 'done' line (e.g. by subprocesses).
        if ($failures != 0 and $rc == 0)
        {
            add_test_line("end", "The test returned success despite having failures");
            $extra_failures++;
        }
        elsif ($failures == 0 and $rc != 0)
        {
            add_test_line("end", "The test returned a non-zero exit code despite reporting no failure");
            $extra_failures++;
        }
    }
    elsif (!defined $rc)
    {
        if (!$last)
        {
            add_test_line("end", "The $dll:$unit done line is missing");
        }
        elsif (-s $report == $maxfilesize)
        {
            mydie "report reached file size limit (>$maxfilesize bytes at $dll:$unit, runaway test?)";
        }
        else
        {
            mydie "report truncated at $dll:$unit (winetest crash?)";
        }
        $extra_failures++;
    }

    $failures += $extra_failures;
    if (!defined $summary)
    {
        $summary = "$s_total $todo " . (($toobig and $failures == 1) ? "big" : $failures) . " $skipped";
    }
    print SUM "- $dll $unit $summary $source\n";
    $testbox->{pattern} = "$dll:$unit" if ($failures);
    if ($failures && ++$failed_units > $maxfailedtests) {
        mydie "too many failed test units (>$maxfailedtests at $dll:$unit)";
    }

    $dll = $unit = $units_re = "";
    %units = ();
    $unitsize = 0;
    $failures = $todo = $skipped = 0;
    $s_failures = $s_todo = $s_skipped = $s_total = 0;
    $extra_failures = $broken = $ignore_exceptions = 0;
    $rc = $summary = undef;
    %pids = ();
}

$line =~ /^Test output:/ or mydie "no test header: $line";
while ($line = <IN>) {
    $unitsize += length($line);
    next if ($line =~ /^\s*$/); # empty lines have no impact
    chomp $line;
    $line =~ s/\r+$//;
    if ($line =~ m%^([_.a-z0-9-]+):([_a-z0-9]+) (start|skipped) (-|[/_.a-z0-9-]+)%)
    {
        my ($l_dll, $l_unit, $l_type, $l_source) = ($1, $2, $3, $4);

        # Close the previous test unit
        close_test_unit(0) if ($dll ne "");

        ($dll, $unit, $source) = ($l_dll, $l_unit, $l_source);
        %units = ($unit => 1);
        $units_re = $unit;

        $testbox = create_test_unit_box();
        if ($l_type eq "skipped")
        {
            add_test_line("skipped", "Skipped by user request");
            $summary = "skipped - - -";
            mydie "too many test units skipped by user request (>$maxuserskips at $dll:$unit)" if ++$skipped_units > $maxuserskips;
            $rc = 0;
        }
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* Subtest ([_.a-z0-9-]+).*)$/) or
           $line =~ /^()([_a-z0-9]+)\.c:(\d+)(:[0-9.]* Subtest ([_.a-z0-9-]+).*)$/)
    {
        my ($pollution, $l_unit, $l_num, $l_text, $l_subunit) = ($1, $2, $3, $4, $5);
        add_test_line("trace", escapeHTML($pollution) .
                               get_source_link($l_unit, $l_num) .
                               escapeHTML($l_text));
        check_unit($l_unit, "subtest");
        $units{$l_subunit} = 1;
        $units_re = join("|", keys %units);
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* Test (?:failed|succeeded inside todo block): .*)$/) or
           $line =~ /^()([_a-z0-9]+)\.c:(\d+)(:[0-9.]* Test (?:failed|succeeded inside todo block): .*)$/)
    {
        my ($pollution, $l_unit, $l_num, $l_text) = ($1, $2, $3, $4);
        add_test_line("failed", escapeHTML($pollution) .
                                get_source_link($l_unit, $l_num) .
                                escapeHTML($l_text));
        check_unit($l_unit, "failure");
        $failures++;
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* Test (?:marked flaky|succeeded inside flaky todo block): .*)$/) or
           $line =~ /^()([_a-z0-9]+)\.c:(\d+)(:[0-9.]* Test (?:marked flaky|succeeded inside flaky todo block): .*)$/)
    {
        my ($pollution, $l_unit, $l_num, $l_text) = ($1, $2, $3, $4);
        add_test_line("flaky", escapeHTML($pollution) .
                                get_source_link($l_unit, $l_num) .
                                escapeHTML($l_text));
        check_unit($l_unit, "flaky");
        $failures++;
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* Test marked todo: .*)$/) or
           $line =~ /^()([_a-z0-9]+)\.c:(\d+)(:[0-9.]* Test marked todo: .*)$/)
    {
        my ($pollution, $l_unit, $l_num, $l_text) = ($1, $2, $3, $4);
        add_test_line("todo", escapeHTML($pollution) .
                              get_source_link($l_unit, $l_num) .
                              escapeHTML($l_text));
        check_unit($l_unit, "todo");
        $todo++;
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* Tests skipped: .*)$/) or
           $line =~ /^()([_a-z0-9]+)\.c:(\d+)(:[0-9.]* Tests skipped: .*)$/)
    {
        my ($pollution, $l_unit, $l_num, $l_text) = ($1, $2, $3, $4);
        add_test_line("skipped", escapeHTML($pollution) .
                                 get_source_link($l_unit, $l_num) .
                                 escapeHTML($l_text));
        # Don't complain and don't count misplaced skips
        $skipped++ if ($units{$l_unit});
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* IgnoreExceptions=([01]).*)$/) or
         $line =~ /^()([_.a-z0-9]+)\.c:(\d+)(:[0-9.]* IgnoreExceptions=([01]).*)$/)
    {
      my ($pollution, $l_unit, $l_num, $l_text, $l_ignore) = ($1, $2, $3, $4, $5);
      add_test_line("", escapeHTML($pollution) .
                        get_source_link($l_unit, $l_num) .
                        escapeHTML($l_text));
      check_unit($l_unit, "IgnoreExceptions");
      $ignore_exceptions = $l_ignore;
    }
    elsif ($line =~ /([0-9a-f]{4}):([_.a-z0-9]+):[0-9.]* unhandled exception [0-9a-fA-F]{8} at /)
    {
        my ($l_pid, $l_unit) = ($1, $2);
        my $class = "";
        if (!$ignore_exceptions)
        {
            $class = "failed";
            if ($units{$l_unit})
            {
                # This also replaces a test summary line.
                $pids{$l_pid || 0} = 1;
                $s_failures++;
            }
            check_unit($l_unit, "unhandled exception");
            $failures++;
        }
        add_test_line($class, escapeHTML($line));
    }
    elsif ($line =~ /Unhandled exception: .* in .* code /)
    {
        my $class = "";
        if (!$ignore_exceptions)
        {
            $class = "failed";
            # This also replaces a test summary line.
            $pids{0} = 1; # the pid is unknown so use 0
            $s_failures++;
            $failures++;
        }
        add_test_line($class, escapeHTML($line));
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* unhandled exception [0-9a-fA-F]{8} in child process ([0-9a-f]{4}).*)$/) or
           $line =~ /^()([_.a-z0-9]+)\.c:(\d+)(:[0-9.]* unhandled exception [0-9a-fA-F]{8} in child process ([0-9a-f]{4}).*)$/)
    {
        my ($pollution, $l_unit, $l_num, $l_text, $l_pid) = ($1, $2, $3, $4, $5);
        my $class = "";
        if (!$ignore_exceptions)
        {
            $class = "failed";
            if ($units{$l_unit})
            {
                # This also replaces a test summary line.
                $pids{$l_pid || 0} = 1;
                $s_failures++;
            }
            check_unit($l_unit, "child exception");
            $failures++;
        }
        add_test_line($class, escapeHTML($pollution) .
                              get_source_link($l_unit, $l_num) .
                              escapeHTML($l_text));
    }
    elsif (($unit ne "" and
            $line =~ /^(.*?)($units_re)\.c:(\d+)(:[0-9.]* .*)$/) or
           $line =~ /^()([_a-z0-9]+)\.c:(\d+)(:[0-9.]* .*)$/)
    {
        my ($pollution, $l_unit, $l_num, $l_text) = ($1, $2, $3, $4);
        add_test_line("trace", escapeHTML($pollution) .
                               get_source_link($l_unit, $l_num) .
                               escapeHTML($l_text));
    }
    elsif ($line =~ /([0-9a-f]{4}):([_a-z0-9]+):[0-9.]* (\d+) tests? executed \((\d+) marked as todo, (?:(\d+) as flaky, )?(\d+) failures?\), (\d+) skipped\./)
    {
        my ($l_pid, $l_unit, $l_total, $l_todo, $l_flaky, $l_failures, $l_skipped) = ($1, $2, $3, $4, $5, $6, $7);

        my $class = $l_failures ? "failed" : $l_todo ? "todo" : "result";
        if ($l_unit eq $unit)
        {
            # There may be more than one summary line due to child processes
            $pids{$l_pid || 0} = 1;
            $s_total += $l_total;
            $s_failures += $l_failures;
            $s_todo += $l_todo;
            $s_skipped += $l_skipped;
            add_test_line($class, escapeHTML($line));
        }
        else
        {
            $class = "failed" if ($l_todo);
            add_test_line($class, escapeHTML($line));
            check_unit($l_unit, "test summary") if ($class ne "result");
        }
    }
    elsif (($dll ne "" and
            $line =~ /(\Q$dll\E):([_a-z0-9]+):([0-9a-f]{4}) done \((-?\d+)\) in /) or
           $line =~ /^([_.a-z0-9-]+):([_a-z0-9]+):([0-9a-f]{4}) done \((-?\d+)\) in /)
    {
        my ($l_dll, $l_unit, $l_pid, $l_rc) = ($1, $2, $3, $4);

        if ($l_dll ne $dll or $l_unit ne $unit)
        {
            # Warn about the missing start line in the current test unit box.
            add_test_line("end", "The $l_dll:$l_unit start line is missing (or it is garbled)");
            $extra_failures++;

            # And close the current test unit taking into account
            # it may have been polluted by the new one.
            $broken = 1;
            close_test_unit(0) if ($dll ne "");

            # Then switch to the new test unit, not for the past lines, but for
            # those before the next 'start' line. This 'new' test unit may have
            # inconsistent results too.
            ($dll, $unit, $source) = ($l_dll, $l_unit, "-");
            %units = ($unit => 1);
            $units_re = $unit;
            $broken = 1;

            $testbox = create_test_unit_box();
            # Finally, warn about the missing start line in the new test unit
            # box.
            add_test_line("end", "The $l_dll:$l_unit start line is missing (or it is garbled)");
            $extra_failures++;
        }

        my $class = $l_rc ? "failed" : "";
        add_test_line($class, escapeHTML($line));

        if ($l_rc == 258)
        {
            add_test_line("end", "Test failed: timed out");
            $summary = "failed 258 - -";
            $extra_failures++;
            $broken = 1;
        }
        elsif ((!$l_pid and !%pids) or ($l_pid and !$pids{$l_pid} and !$pids{0}))
        {
            # The main summary line is missing
            if ($l_rc & 0xc0000000)
            {
                add_test_line("end", sprintf("Test failed: crash (%08x)", $l_rc & 0xffffffff));
                $summary = "failed crash - -";
                $extra_failures++;
                $broken = 1;
            }
            elsif (!$broken)
            {
                add_test_line("end", "The main process has no test summary line");
                $extra_failures++;
            }
        }
        elsif ($l_rc & 0xc0000000)
        {
            add_test_line("end", sprintf("Test failed: crash (%08x)", $l_rc & 0xffffffff));
            $summary = "failed crash - -";
            $extra_failures++;
            $broken = 1;
        }
        $rc = $l_rc;
    }
    else
    {
        add_test_line("trace", escapeHTML($line));
    }
}
close_test_unit(1);

close SUM or mydie "error writing to '$tmpdir/summary.txt': $!";
close IN;

mydie "too few tests run (", @boxes-1, ")" if @boxes <= $maxfailedtests * 5;
mydie "report reached file size limit (runaway test?)" if -s $report >= $maxfilesize;


#
# Generate the 'DLL version' section of the info box
#

$box->{data} .= "<table class=\"output\">\n";
foreach my $dll (sort keys %dllinfo)
{
    $box->{data} .= sprintf "<tr id=\"%s\">", escapeHTML($dll);
    if ($dllinfo{$dll}->{version} =~ /^dll is missing/)
    {
        my $reason = $dllinfo{$dll}->{version};
        $reason =~ s/dll is //;
        $box->{data} .= sprintf "<td>%s</td><td class=\"skipped\">%s</td></tr>\n", escapeHTML($dll), escapeHTML($reason);
    }
    elsif ($dllinfo{$dll}->{version} eq "skipped")
    {
        $box->{data} .= sprintf "<td>%s</td><td class=\"skipped\">skipped by user request</td></tr>\n", escapeHTML($dll);
    }
    elsif ($dllinfo{$dll}->{version} eq "dll is a stub")
    {
        $box->{data} .= sprintf "<td>%s</td><td class=\"skipped\">stub</td></tr>\n", escapeHTML($dll);
    }
    elsif ($dllinfo{$dll}->{version} eq "dll is native")
    {
        # There should be no native dll in the Wine tests
        $box->{data} .= sprintf "<td>%s</td><td class=\"failed\">native</td></tr>\n", escapeHTML($dll);
    }
    elsif ($dllinfo{$dll}->{version} eq "load error 258")
    {
        $box->{data} .= sprintf "<td>%s</td><td class=\"failed\">timed out getting the test list</td></tr>\n",
                                   escapeHTML($dll);
    }
    elsif ($dllinfo{$dll}->{version} eq "load error 225")
    {
        $box->{data} .= sprintf "<td>%s</td><td class=\"failed\">test blocked by the anti-virus</td></tr>\n",
                                   escapeHTML($dll);
    }
    elsif ($dllinfo{$dll}->{version} =~ /^load error (\d+)$/)
    {
        # Flag unknown errors
        $box->{data} .= sprintf "<td>%s</td><td class=\"failed\">got error %s while getting the test list</td></tr>\n",
                                   escapeHTML($dll), escapeHTML($1);
    }
    elsif (defined($dllinfo{$dll}->{first}))
    {
        $box->{data} .= sprintf "<td><a href=\"report.html#%s\">%s</a></td><td>%s</td></tr>\n",
                                    escapeHTML($dllinfo{$dll}->{first}), escapeHTML($dll), escapeHTML($dllinfo{$dll}->{version});
    }
    else
    {
        $box->{data} .= sprintf "<td>%s</td><td>%s</td></tr>\n", escapeHTML($dll), escapeHTML($dllinfo{$dll}->{version});
    }
}
$box->{data} .= "</table>";


#
# Link the boxes together
#

for (my $i = 0; $i <= $#boxes; $i++)
{
    $boxes[$i]->{prev} = $i > 0 ? $boxes[$i-1]->{id} : "";
    $boxes[$i]->{next} = $boxes[$i+1]->{id} if $i < $#boxes;
}


#
# Create the 'full report' page
#

open REPORT, ">$tmpdir/report.html" or mydie "could not open '$tmpdir/report.html' for writing: $!";

print REPORT start_html( -title => "$short_date $tag report", -style => {src=>"/report.css"} );
print REPORT "<div class=\"navbar\">";
print REPORT "<a href=\"report\">raw report</a> | <a href=\"..\">summary</a> | <a href=\"../..\">index</a>";
print REPORT "</div>\n";

for (my $i = 0; $i <= $#boxes; $i++)
{
    printf REPORT "<div id=\"%s\" class=\"%s\">\n", $boxes[$i]->{id}, $boxes[$i]->{class};
    printf REPORT "<div class=\"updownbar\">%s<div class='ralign'>", $boxes[$i]->{title};
    if ($boxes[$i]->{pattern})
    {
        printf REPORT "<a href=\"/data/patterns.html#%s\">pattern</a> | ", $boxes[$i]->{pattern};
    }
    if ($boxes[$i]->{testname})
    {
        printf REPORT "<a href='/data/tests/%s.html'>all results</a> | ", $boxes[$i]->{testname};
    }
    printf REPORT "<a href=\"#%s\">&uarr;</a>", $boxes[$i]->{prev} if defined $boxes[$i]->{prev};
    printf REPORT "<a href=\"#%s\">&darr;</a>", $boxes[$i]->{next} if defined $boxes[$i]->{next};
    print REPORT "</div></div>\n";
    print REPORT $boxes[$i]->{data}, "</div>\n";
}
print REPORT end_html();
close REPORT or mydie "error writing to '$tmpdir/report.html': $!";


#
# Create the information and individual test unit pages
#

for (my $i = 0; $i <= $#boxes; $i++)
{
    open FILE, ">$tmpdir/$boxes[$i]->{id}.html" or mydie "could not open '$tmpdir/$boxes[$i]->{id}.html' for writing: $!";
    print FILE start_html( -title => "$short_date $boxes[$i]->{id} $tag", -style => {src=>"/report.css"} );
    print FILE "<div class=\"navbar\">";
    print FILE $boxes[$i]->{prev} ? "<a href=\"./$boxes[$i]->{prev}.html\">prev</a> | " : "prev | ";
    print FILE $boxes[$i]->{next} ? "<a href=\"./$boxes[$i]->{next}.html\">next</a> | " : "next | ";
    print FILE "<a href=\"version.html\">info</a> | ";
    printf FILE "<a href=\"report.html%s\">full report</a> | ", $i ? "#$boxes[$i]->{id}" : "";
    print FILE "<a href=\"report\">raw report</a> | ";
    print FILE "<a href=\"..\">summary</a> | <a href=\"../..\">index</a></div>\n";

    printf FILE "<div id=\"%s\" class=\"%s\">\n", $boxes[$i]->{id}, $boxes[$i]->{class};
    printf FILE "<div class=\"updownbar\">%s<div class='ralign'>", $boxes[$i]->{title};
    if ($boxes[$i]->{pattern})
    {
        printf FILE "<a href=\"/data/patterns.html#%s\">pattern</a> | ", $boxes[$i]->{pattern};
    }
    if ($boxes[$i]->{testname})
    {
        printf FILE "<a href='/data/tests/%s.html'>all results</a>", $boxes[$i]->{testname};
    }
    print FILE "</div></div>\n";
    print FILE $boxes[$i]->{data}, "</div>\n";
    print FILE end_html();
    close FILE or mydie "error writing to '$tmpdir/$boxes[$i]->{id}.html': $!";
}


#
# Move the files into place
#

use Errno qw/ENOTEMPTY/;

my $builddir = "data/$testbuild";
if (!$update) {
    foreach my $dir ("data", $builddir) {
        if (!-d $dir && !mkdir $dir) {
            print STDERR "$name0:error: unable to create the '$dir' directory: $!\n";
            exit 3;
        }
    }

    my ($dir, $dirbase, $try);
    $dir = $dirbase = "$builddir/${version}_$tag";
    $try = 0;
    while (!rename $tmpdir, $dir) {
        $!{ENOTEMPTY} or mydie "could not rename '$tmpdir' to '$dir': $!";
        ++$try < $maxmult or mydie "more than $maxmult submissions for $shortbuild/$version";
        $dir = "${dirbase}_$try";
    }
}
if (!-e "$builddir/outdated") {
    if (!open SIGN, ">$builddir/outdated") {
        error("could not open '$builddir/outdated' for writing: $!\n");
        exit 1;
    }
    close SIGN;
}
