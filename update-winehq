#!/usr/bin/perl -w
#
# Update the winehq.org homepage from the ANNOUNCE file of a new Wine release
#
# Copyright 2006 Alexandre Julliard
#
# Usage: update-winehq release [announce-file]
#
# Must be run from the top-level dir of a git checkout of the website module
#

use strict;
use open ':utf8';

sub xml_escape($)
{
    my $str = shift;
    $str =~ s/&/&amp;/g;
    $str =~ s/</&lt;/g;
    $str =~ s/>/&gt;/g;
    my @chars = unpack "U*", $str;
    $str = join "", map { ($_ > 127) ? sprintf "&#%u;", $_ : chr($_); } @chars;
    $str =~ s/(http:\/\/\S+)/<a href=\"$1\">$1<\/a>/g;
    return $str;
}

my $rel = $ARGV[0];
my @text;

die "Not in website dir?" unless -d "news/en";
system("git", "pull") == 0 or die "git pull failed";

# Parse the ANNOUNCE file

open ANNOUNCE, $ARGV[1] || "ANNOUNCE" or die "cannot open ANNOUNCE";
my $ul = 0;
my $url;

push @text, "<body>\n<p>";

while (<ANNOUNCE>)
{
    chomp;
    if (/^What's new in this release/)
    {
        push @text, " <a href=\"{\$root}/announce/$rel\">What's new</a> in this release:";
        next;
    }
    elsif (/the release notes/)
    {
        xml_escape($_);
        s/the release notes/the <a href=\"{\$root}\/announce\/$rel\">release notes<\/a>/;
        push @text, " " . $_;
        next;
    }
    if (/^\s*$/)
    {
        if ($ul)
        {
            push @text, "</li>\n</ul>\n";
            $ul = 0;
        }
        push @text, "</p>\n<p>";
        next;
    }
    if (/^ *- (.*)$/)
    {
        push @text, (!$ul ? "\n<ul>\n  <li>" : "</li>\n  <li>") . xml_escape($1);
        $ul = 1;
        next;
    }
    last if (/^The source is available/);
    last if (/^--------------------/);
    push @text, " " . xml_escape($_);
}

while (<ANNOUNCE>)
{
    if (/https?:(\/\/dl.winehq.org\/wine\/source\/.*\/wine-$rel\..*)$/)
    {
        $url = $1;
        last;
    }
}
die "Download URL not found" unless $url;

push @text, "The source is <a href=\"$url\">available now</a>.\n";
push @text, "Binary packages are in the process of being built, and will appear soon at their respective <a href=\"{\$root}/download\">download locations</a>.\n";
push @text, "</p></body></news>\n";
close ANNOUNCE;

# Create the news file

my $newsdate = `date +%Y%m%d`;
chomp $newsdate;
my $newscount = 1;
my $newsfile;

do
{
    $newsfile = sprintf "news/en/%s%02u.xml", $newsdate, $newscount++;
}
while (-f "$newsfile");

open NEWS, ">$newsfile" or die "cannot create $newsfile";

my $date = `date "+%B %e, %Y"`;
chomp $date;

print NEWS <<EOF;
<news>
<date>$date</date>
<title>Wine $rel Released</title>
EOF

print NEWS join "", @text;
close NEWS;
print "Created $newsfile\n";

# Commit changes

if (system("git", "add", $newsfile))
{
    unlink $newsfile;
    die "git add $newsfile failed";
}

if (system("git", "commit", "-s", "-m", "Wine release $rel", $newsfile))
{
    system("git", "rm", "-f", $newsfile);
    die "git commit failed";
}

system("git", "push") == 0 or die "git push failed";
