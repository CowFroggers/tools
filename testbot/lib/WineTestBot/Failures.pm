# -*- Mode: Perl; perl-indent-level: 2; indent-tabs-mode: nil -*-
# Copyright 2022 Francois Gouget
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA

use strict;


package WineTestBot::Failure;

=head1 NAME

WineTestBot::Failure - Describes a known Wine test failure

=head1 DESCRIPTION

Failure objects document known test failures and link them to the corresponding
bug describing the issue. They are mainly used to avoid reporting a test
failure as new when it has in fact happened before despite not being present
in the reference WineTest logs, either because it is just too rare, or because
its text is ever changing.

Finally Failure objects are linked to the Task+Log they match by means of the
TaskFailure objects.

=cut

use WineTestBot::WineTestBotObjects;
our @ISA = qw(WineTestBot::WineTestBotItem);

use WineTestBot::Engine::Notify;


sub Compare($$)
{
  my ($self, $B) = @_;

  # Sort deleted entries last
  my %StatusOrders = ("deleted" => 1);

  return ($StatusOrders{$self->GetColValue("BugStatus")} || 0) <=> ($StatusOrders{$B->GetColValue("BugStatus")} || 0) ||
         $self->GetColValue("ErrorGroup") cmp $B->GetColValue("ErrorGroup") ||
         $self->GetColValue("TestUnit") cmp $B->GetColValue("TestUnit") ||
         $self->GetColValue("BugDescription") cmp $B->GetColValue("BugDescription") ||
         $self->GetColValue("Notes") cmp $B->GetColValue("Notes");
}

sub BugId($;$)
{
  my ($self, $NewBugId) = @_;

  my $CurrentBugId = $self->SUPER::BugId;
  return $CurrentBugId if (!defined $NewBugId);

  if (!defined $CurrentBugId or $NewBugId ne $CurrentBugId)
  {
    $self->{OldBugId} = $CurrentBugId if (!defined $self->{OldBugId});
    $self->SUPER::BugId($NewBugId);
  }

  return $NewBugId;
}

sub OnSaved($)
{
  my ($self) = @_;

  $self->SUPER::OnSaved();
  if ((defined $self->{OldBugId} and $self->{OldBugId} ne $self->BugId) or
      !$self->BugStatus or !$self->BugDescription)
  {
    UpdateFailure($self->Id);
  }
  delete $self->{OldBugId};
}

sub Validate($)
{
  my ($self) = @_;

  if ($self->ErrorGroup =~ /^ / or $self->ErrorGroup =~ / $/)
  {
    return ("ErrorGroup", "The error group '". $self->ErrorGroup ."' should not have leading or trailing spaces");
  }
  if ($self->TestUnit !~ /^[_a-z0-9]*$/)
  {
    return ("TestUnit", "The test unit '". $self->TestUnit ."' contains invalid characters");
  }
  foreach my $Field ("ConfigRegExp", "FailureRegExp")
  {
    my $RegExp = $self->$Field;
    my $ErrMessage = eval {
      use warnings FATAL => qw(regexp);
      if ($RegExp and "" =~ /$RegExp/)
      {
        return "The regular expression should not match empty strings";
      }
    } || "$@";
    if ($ErrMessage)
    {
      my $DisplayName = $self->GetPropertyDescriptorByName($Field)->GetDisplayName();
      return ($Field, "$DisplayName: $ErrMessage");
    }
  }
  return $self->SUPER::Validate();
}


package WineTestBot::Failures;

=head1 NAME

WineTestBot::Failures - A Failure collection

=head1 DESCRIPTION

This collection contains all known failures.

=cut

use Exporter 'import';
use WineTestBot::WineTestBotObjects;
BEGIN
{
  our @ISA = qw(WineTestBot::WineTestBotCollection);
  our @EXPORT = qw(CreateFailures);
}

use ObjectModel::BasicPropertyDescriptor;
use ObjectModel::EnumPropertyDescriptor;
use ObjectModel::DetailrefPropertyDescriptor;
use WineTestBot::TaskFailures;


sub CreateItem($)
{
  my ($self) = @_;

  return WineTestBot::Failure->new($self);
}

my @PropertyDescriptors = (
  # Rather than using a combination of the other fields, give each entry a
  # unique id. This allows having multiple entries for a single Wine bug in
  # case there are too many failures to match for the regular expression to
  # fit in a single regular expression field. This also avoids using the
  # regular expression as part of the primary key which would be troublesome
  # as it may need to be adjusted in case it is buggy or if the test changes.
  CreateBasicPropertyDescriptor("Id", "Id", 1, 1, "S",  10),

  # Identify the error group the failure can occur in. This is usually a
  # test module but it may also be a group containing extra errors. Also some
  # log files have a single nameless error group.
  CreateBasicPropertyDescriptor("ErrorGroup", "Test module", !1, !1, "A", 64),
  # For test modules, identify the test unit the failure can occur in. In the
  # other cases this should be an empty string.
  CreateBasicPropertyDescriptor("TestUnit", "Test unit", !1, !1, "A", 32),

  # A regular expression to match the configurations the failure can happen in.
  # A configuration name is of the form 'VMNAME:REPORTNAME'.
  CreateBasicPropertyDescriptor("ConfigRegExp", "Configurations RegExp", !1, !1, "A", 64),

  # A regular expression to match the troublesome failures.
  CreateBasicPropertyDescriptor("FailureRegExp", "Failures RegExp", !1, 1, "A", 256),

  # Can be used for documentation when multiple entries are needed to match
  # all the failures associated with a given bug.
  # Can also be used to document when a regular expression has been modified,
  # for instance to match changes in a test.
  CreateBasicPropertyDescriptor("Notes", "Notes", !1, !1, "A", 128),

  # Record when an entry was last identified as a new or old failure, even
  # after the corresponding tasks have expired (see the TaskFailures table).
  CreateBasicPropertyDescriptor("LastNew", "Last new", !1, !1, "DT", 19),
  CreateBasicPropertyDescriptor("LastOld", "Last old", !1, !1, "DT", 19),

  # Every entry must be associated with a Wine bug.
  # Note: The 'deleted' bug status means this failure entry should be
  # deleted as soon as it is not referenced anymore.
  CreateBasicPropertyDescriptor("BugId", "Bug id", !1, 1, "N", 10),
  CreateBasicPropertyDescriptor("BugStatus", "Bug status", !1, !1, "A", 32),
  CreateBasicPropertyDescriptor("BugDescription", "WineHQ Bug description", !1, !1, "A", 128),

  CreateDetailrefPropertyDescriptor("TaskFailures", "Tasks", \&CreateTaskFailures),
);
SetDetailrefKeyPrefix("Failure", @PropertyDescriptors);

=pod
=over 12

=item C<CreateFailures()>

Creates a collection of Failure objects.

=back
=cut

sub CreateFailures(;$)
{
  my ($ScopeObject) = @_;
  return WineTestBot::Failures->new("Failures", "Failures", "Failure",
                                    \@PropertyDescriptors, $ScopeObject);
}

1;
