<?php
/**
 * Redirects to a URL for the Wine Mono installer
 */

// Default version if none given
$sVersion = '4.6.0';

// Suffix appended to base name of file
$sFileSuffix = '';

// Folder which contains files
$sFolder = 'wine-mono';

// Check if a specific version was passed
if(isset($_GET['v'])) {
	$sVersion = $_GET['v'];
}

// Check which arch to use
if(((int)(explode(".", $sVersion, 2)[0])) >= 5) {
	if(!isset($_GET['arch']) || $_GET['arch'] == 'x86_64')
		$sArch = 'x86';
	else
		$sArch = $_GET['arch'];
	$sFileSuffix = $sVersion.'-'.$sArch;
}
else // version < 5, no arch in filename
	$sFileSuffix = $sVersion;

$sExt = 'msi';

// Name of the file
$sFileName = sprintf('%s/%s/wine-mono-%s.%s', $sFolder, $sVersion, $sFileSuffix, $sExt);

// Common code for Wine downloader scripts
require("download.inc.php");
?>
